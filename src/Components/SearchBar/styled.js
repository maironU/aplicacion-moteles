import styled from 'styled-components'

export const ContainerSearchBar = styled.div`
	width: 100%;
	position: fixed;
	display: flex;
	flex-flow: row nowrap;
	align-items: center;
	padding: 12px;
	background: white;
	z-index: 15;
	top: 0;
`
export const ContainerIconLocation = styled.div`
	font-size: 25px;
`
export const ContainerInput = styled.div`
	width: 100%;
	font-size: 10px;
`
export const Input = styled.input`
	color: #48484D;
	width: 100%;
	border: none;
	margin-left: 10px;
	:focus{
		outline: none;
	}
`
export const CitiesResultList = styled.ul`
	padding: 0;
	width: 100%;
	background: white;
	margin: 0;
	position: fixed;
	z-index: 10;
	display: block;
`
export const ContainerCityResultItem = styled.li`	
	color: #48484D;
	list-style: none;
	display: flex;
	flex-direction: row;
	padding: 15px;
	border-top: 0.2px solid #ADADB7	;
`
export const CityResultItem = styled.span`
	margin-left: 15px;		

`
