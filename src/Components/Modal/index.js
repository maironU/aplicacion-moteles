import React,{useContext,useState, useEffect, useRef} from 'react'
import {TheContext} from '../../Context'
import Fullscreen from "react-full-screen";
import {ContainerModal, Container, ContainerAll, ContainerLogo, Logo, Name, Date,
 ContainerContentHistory, ImgContentHistory, ContainerImg, ContainerVideo, VideoContentHistory,ContainerArrows,
  MoveLeft, MoveRight, ContainerToPlay} from './styled'
import { MdClose } from 'react-icons/md'
import { FaPlay } from 'react-icons/fa'
import CProgressBar from '../ProgressBar'
import { Link, Redirect} from "react-router-dom";


const Modal = () => {

	let {setHistoryList, Historias, isOpenModal, setIsOpenModal, HistoryMotel, toggleFullScreen, toPlay, setToPlay, ip} = useContext(TheContext)

	let [to, setTo] = useState(false)
	let [count, setCount] = useState(0)

	console.log(toPlay)

	useEffect(() => {
		if(toPlay === true){
			var timer = setInterval(() => {
			    if(count > Historias.length - 2){
						setTo(true)
						toggleFullScreen()
					}else{
						setCount(count + 1)
					}
			}, 5000);
		}
		return () => clearInterval(timer)
	})

	const Play = () => {
		setToPlay(true)
		toggleFullScreen()
	}

	return(
		<ContainerModal>
			{to &&
				<Redirect to='/'/>
			}
			<Container>
				<CProgressBar cont={count}/>
				<ContainerAll>
					<ContainerLogo>
						<Logo src={ip + `api/almacenamiento/${HistoryMotel.logo}`} />
						<Name>{HistoryMotel.motel}</Name>
						<Date>23-02-2020</Date>
					</ContainerLogo>

					<Link to="/" onClick ={(e) => toggleFullScreen()}>
						<MdClose size={25} fill='white'/>
					</Link>
				</ContainerAll>
				{toPlay === true ?
					<ContainerContentHistory>
						<ContainerImg opacity = "none" >
							<ImgContentHistory src={ip + `api/almacenamiento/${Historias[count].historymotel}`} />
						</ContainerImg>

						<ContainerArrows>
							<MoveLeft onClick={(e) => count  > 0 && setCount(count -1) }></MoveLeft>
							<MoveRight onClick={(e) => count < Historias.length - 1 && setCount(count + 1) }></MoveRight>
						</ContainerArrows>
					</ContainerContentHistory>	
					:
					<ContainerContentHistory>
						<ContainerImg opacity = "0.2" >
							<ImgContentHistory src={ip + `api/almacenamiento/${Historias[count].historymotel}`} />
						</ContainerImg>

						<ContainerToPlay onClick ={(e) => Play()}>
							<FaPlay size={25} />
						</ContainerToPlay>
					</ContainerContentHistory>	
				}
			</Container>
		</ContainerModal>
	)
}
//

export default Modal

//src={`http://localhost:8000/api/almacenamiento/${historias[i].historymotel}`}