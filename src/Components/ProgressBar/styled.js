import styled from 'styled-components'

export const ContainerBar = styled.div`
	margin-top: 10px;
	display: flex;
	flex-direction: row;
	justify-content: center;
`
