import styled from 'styled-components'
	
export const ContainerInformation = styled.div`
	width: 94%;
	margin: 0 auto;
	margin-top: 50px;
`

export const Information = styled.p`
	width: 100%;
	text-align: justify;
`