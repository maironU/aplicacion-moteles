import React, {useState} from 'react'

const TheContext = React.createContext()
const {Provider, Consumer} = TheContext

const Context = ({children}) => {

	let [filter, setFilter] = useState([])
	let [historyList, setHistoryList] = useState([])
	let [isOpenModal, setIsOpenModal] = useState(false)
	let [id, setId] = useState(0)
	let [Historias, setHistorias] = useState([])
	let [HistoryMotel, setHistoryMotel] = useState([])
	let [MotelsForCity, setMotelsForCity] = useState([])
	let [count, setCount] = useState(0)
	let [noPeticion, setNoPeticion] = useState(false)
	let [toPlay, setToPlay] = useState(true)
	let [images, setImages] = useState([])
	let [isOpenModalTariffs, setIsOpenModalTariffs] = useState(false)
	let [habitacionImage, setHabitacionImage] = useState([])
	let [price, setPrice] = useState("")
	let [tarifas, setTarifas] = useState([])

	let ip = 'http://192.168.1.5:8000/'

	function toggleFullScreen() {
	  if (!document.fullscreenElement) {
	      document.documentElement.requestFullscreen();
	      console.log("Com")
	  }else {
	    if (document.exitFullscreen) {
	      	console.log("No Com")
	      	document.exitFullscreen(); 
	    }
	  }
	}

	return(
		<Provider value={{filter, setFilter,historyList, setHistoryList, isOpenModal, setIsOpenModal, 
		 id, setId, Historias, setHistorias, HistoryMotel, setHistoryMotel, MotelsForCity,
		 setMotelsForCity, toggleFullScreen, count, setCount, noPeticion, setNoPeticion, toPlay, setToPlay,
		 images, setImages, ip, isOpenModalTariffs, setIsOpenModalTariffs, habitacionImage, setHabitacionImage,
		 price, setPrice, tarifas, setTarifas
		}}>
			{children}
		</Provider>
	)
}

export {Context, Consumer, TheContext}