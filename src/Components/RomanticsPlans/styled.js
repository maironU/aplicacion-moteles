import styled from 'styled-components'

export const ContainerRomanticsPlans = styled.div`
	width: 95%;
	margin: 0 auto;
`
export const ContainerRomanticPlan = styled.div`
	width: 100%;
	display: flex;
	font-size: 10px;
	align-items: center;
	margin-top: 10px;
`
export const ContainerTriangle = styled.div`
	height: 8px;
	width: 8px;
	padding: 4px;
	background: #F0F0F0;
	display: flex;
	justify-content: center;
	align-items: center;
	border-radius: 5px;
	margin-right: 6px;
`
export const Triangle = styled.div`
	width: 0;
    height: 0;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    border-bottom: 8px solid #037501;
`
export const ContainerInfoPlan = styled.div`
	
`
export const Plan = styled.span`
`