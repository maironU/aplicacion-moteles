import styled from 'styled-components'



export const ContainerImageItem = styled.li`
	min-width: 100%;
	height: 270px;
`
export const Image = styled.img`
	width: 100%;
	height: 100%;
`
/*export const ContainerCircles = styled.div`
	display: flex;
	flex-direction: row;
	position: absolute;
	bottom: 10px;
	left: 47%;
	z-index: 100;
`
export const Circle = styled.div`
	width: 7px;
	height: 7px;
	border-radius: 50%;
	background: white;
	margin-left: 3px;
	border: 0.5px solid #5B5757;
`*/
