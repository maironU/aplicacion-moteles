import React, {useState, useEffect, useContext } from 'react'
import {ContainerSearchBar, ContainerInput, Input, CitiesResultList, CityResultItem, ContainerCityResultItem} from './styled'
import { MdLocationSearching } from 'react-icons/md'
import { MdLocationOn } from 'react-icons/md'
import {TheContext} from '../../Context'
import MotelItem from '../MotelItem'

const SearchBar = () => {

	let {setFilter, ip } = useContext(TheContext)

	const [ cities, setCities ] = useState([])
	const [ city, setCity ] = useState("")

	useEffect(() => {
		window.fetch(ip + `api/ciudades/mostrar/${city}`)
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
			setCities(myJson);
		})
	},[city])

	const filterMotels = ($key, $myJson) => {

		setCity($myJson.namecity)
		var ocultar = document.getElementById('ocultar')
		ocultar.style.display = "none";
		setFilter($myJson)
	}

	const MostrarListCity = () => {
		var mostrar = document.getElementById('ocultar')
		mostrar.style.display = "block";
	}

	return(
		
		<>
			<ContainerSearchBar>

				<MdLocationSearching size={20} fill='#48484D' />

				<ContainerInput>
					<Input autocomplete="off" value={city} onChange={(e) => setCity(e.target.value)} name="city" type="text" placeholder="Buscar ciudad" 
					onClick = {(e) => MostrarListCity()}/>
				</ContainerInput>

			</ContainerSearchBar>

			<CitiesResultList id="ocultar">
				{cities.map((city, i) => {
					return(
						<ContainerCityResultItem key={i} onClick={(e) => filterMotels(i, cities[i])}>
							<MdLocationOn />
							<CityResultItem>{city.namecity}</CityResultItem>
						</ContainerCityResultItem>
						)				
			})}
			</CitiesResultList>
		</>
	)
}

export default SearchBar