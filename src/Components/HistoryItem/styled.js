import styled from 'styled-components'
import { Link as LinkRouter } from 'react-router-dom'

export const ContainerItem = styled.li`
	display: flex;
	flex-direction: column;
	list-style: none;
	align-items: center;
	margin-left: 11px;
	margin-top: 11px;	
`

export const ContainerAll = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: 68px;
	height: 68px;
	border-radius: 50%;
	background: -moz-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -webkit-gradient(left top,right top,color-stop(0%,rgba(51,68,131,1)),color-stop(100%,rgba(110,35,104,1)));
    background: -webkit-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -o-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -ms-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: linear-gradient(to right,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    -webkit-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
`
export const Link = styled(LinkRouter)`
	background: white;
	display: flex;
	justify-content: center;
	align-items: center;
	width: 64px;
	height: 64px;
	border-radius: 50%;
`

export const Image = styled.img`
	width: 60px;
	height: 60px;
	border-radius: 50%;
`
export const Name = styled.span`
	max-width: 56px;
	max-height: 13px;
	font-size: 11px;
	overflow: hidden;
	white-space:nowrap;
    text-overflow: ellipsis;
`
export const ContainerName = styled.div`
	display: flex;
	align-items: center;
	padding: 3px;
`