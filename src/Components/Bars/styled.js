import styled, {keyframes} from 'styled-components'

export const Bar = styled.div`
	width: ${props => props.width}%;
	height: 3px;
	background: grey;
	margin-left: 5px;
`
const animationProgressBar= keyframes`
  0% {width:0%;} 100% {width: 100%;transition: width 5s linear;}
`

export const ProgressBar = styled.div`
	width: 0%;
	height: 100%;
	background: white;
	animation: ${animationProgressBar} 5s linear;
`