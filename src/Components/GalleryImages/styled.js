import styled from 'styled-components'

export const List = styled.ul`
	position: sticky;
	margin: 0;
	padding: 0;
	width: 100%;
	display: flex;
	top: 45px;
	flex-direction: row;
	overflow: scroll;

	::-webkit-scrollbar {
    	display: none;
	}
`

export const ContainerPrice = styled.div`
	font-size: 18px;
	display: inline-block;
	padding: 10px;
	color: white;
	background: black;
	opacity: 0.7;
	position: fixed;
	left: 0;
	top: 45px;
	z-index: 100;
`
export const Price = styled.span`

`

