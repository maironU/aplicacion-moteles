import styled from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom';

export const Link = styled(LinkRouter)`	
	width: 100%;
	height: 250px;
	list-style: none;
	position: relative;
	display: flex;
	flex-direction: column;
	margin-top: 10px;
	text-decoration: none;
`
export const Price = styled.span`

`
export const Image = styled.img`
	width: 100%;
	height: 80%;
`
export const Name = styled.span`
	
`
export const ContainerPrice = styled.div`
	font-size: 18px;
	display: inline-block;
	padding: 10px;
	color: white;
	background: black;
	opacity: 0.7;
	position: absolute;
	left: 0;
`
export const ContainerName = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;
	color: white;
	padding: 4px;
	background-color: red; 
 	background: -moz-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -webkit-gradient(left top,right top,color-stop(0%,rgba(51,68,131,1)),color-stop(100%,rgba(110,35,104,1)));
    background: -webkit-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -o-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: -ms-linear-gradient(left,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    background: linear-gradient(to right,rgba(51,68,131,1) 0%,rgba(110,35,104,1) 100%);
    -webkit-filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#334483',endColorstr='#6e2368',GradientType=1 );
`
export const ContainerArrowRight = styled.div`
	display: flex;
	align-items: center;
`
