import React, {useContext, useState, useEffect, useRef} from 'react'
import {Bar, ProgressBar} from './styled'
import {TheContext} from '../../Context'

const Bars = ({id, cont}) => {

	let {Historias, toPlay} = useContext(TheContext)

	const width = 90/Historias.length
	let [count, setCount] = useState(0)

	return(
		<>
			<Bar width={width}>
				{id < cont &&
					<Bar style={{background:'white',margin:'0'}}></Bar>
				}
				{(id === cont && toPlay === true) &&
					<ProgressBar></ProgressBar>
				}
			</Bar>
		</>
	)
}

export default Bars