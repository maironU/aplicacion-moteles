import React from 'react'
import {ContainerInfoBar, ContainerBackName, Name, Share, ContainerBackArrow } from './styled'
import { MdKeyboardArrowLeft} from 'react-icons/md'
import { FiShare} from 'react-icons/fi'

const InfoBar = ({motel}) =>{

	return(
		<ContainerInfoBar>
			<ContainerBackName>
				<ContainerBackArrow onClick = {() => window.history.back()}>
					<MdKeyboardArrowLeft size={25}/>
				</ContainerBackArrow>
				<Name>Motel {motel}</Name>
			</ContainerBackName>

			<Share>
				<FiShare size={18}/>
			</Share>
		</ContainerInfoBar>
	)
}

export default InfoBar