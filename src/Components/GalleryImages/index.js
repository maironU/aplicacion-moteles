import React, {useState, useEffect, useContext} from 'react'
import {List, ContainerPrice, Price} from './styled'
import ImageItem from '../ImageItem'
import {TheContext} from '../../Context'

const GalleryImages = () => {

	let {habitacionImage, price} = useContext(TheContext)

	useEffect(() => {
		let id = document.getElementById("ul")
		console.log(id.scroll)
		id.scrollLeft = 0
	},[habitacionImage])

	return(
		<List id = "ul">
			<ContainerPrice>
				<Price>{price} - $100.000</Price>
			</ContainerPrice>
	        { habitacionImage.map( (elem, key ) => <ImageItem key={key} {...elem}/> ) }
	    </List> 
	)
}

export default GalleryImages