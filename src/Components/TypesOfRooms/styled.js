import styled from 'styled-components'

export const ContainerTipo = styled.div`
	width: 95%;
	margin: 0 auto;
	display: flex;
	flex-direction: column;
`
export const Tipo = styled.div`
	margin-bottom: 10px;
`
export const ContainerNamePrice = styled.div`
	font-size: 13px;
	display: flex;
	justify-content: space-between;
	margin-bottom: 10px;
	margin-top: 10px;
`
export const Name = styled.span`
`
export const ContainerPriceTariffs = styled.div`
	display: flex;
	align-items: center;
`
export const Tariffs = styled.div`
	cursor: pointer;
	color: #004495;
	font-size: 17px;
`
export const Price = styled.span`
	margin-right: 5px;
`
export const ContainerHabitaciones = styled.div`
	width: 100%;
	display: flex;
	overflow: scroll;

	::-webkit-scrollbar {
    	display: none;
	}
`
