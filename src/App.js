import React from 'react';
import {Context} from './Context'
import Home from './Pages/Home'
import History from './Pages/History'
import InfoMotel from './Pages/InfoMotel'
import GlobalStyled from './Styled/GlobalStyled'
import { BrowserRouter as Router , Switch, Route} from "react-router-dom";

function App() {

  return (
    <>
    	<Context>
    	<GlobalStyled  />
	      	<Router>
				<Switch>
		          <Route exact path="/" component= {Home} />
		          <Route exact path="/historia/:motel" component = {History} />
		          <Route exact path="/:motel" component = {InfoMotel} />
		        </Switch>
			</Router>
		</Context>
    </>
  );
}

export default App;
