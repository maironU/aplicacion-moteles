import React, {useContext} from 'react'
import { ContainerNroLink, NroHabitacion, Link } from './styled'
import {TheContext} from '../../Context'

const Habitaciones = ({habitacion, imagen}) => {

	let {habitacionImage, setHabitacionImage} = useContext(TheContext)

	const ImagesHabitacion = () => {
		if(habitacionImage.length > 3){
			let rango =imagen.length - habitacionImage.length 

			if(rango > 0){
				let array = habitacionImage.slice(-(rango))
				setHabitacionImage(imagen.concat(array))
			}else{
				let array = habitacionImage.slice(rango)
				setHabitacionImage(imagen.concat(array))
			}
		}else{
			setHabitacionImage(imagen.concat(habitacionImage))
		}
	}

	return(
		<ContainerNroLink>
			<NroHabitacion>
				<b>{habitacion}</b>
			</NroHabitacion>
			<Link onClick = {()=> ImagesHabitacion()}>
				ver
			</Link>
		</ContainerNroLink>
	)
}

export default Habitaciones