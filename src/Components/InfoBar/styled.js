import styled from 'styled-components'

export const ContainerInfoBar = styled.div`
	width: 100%;
	position: fixed;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	background: white;
	padding: 10px;
	top: 0;
`
export const ContainerBackName = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
`
export const Name = styled.span`
`
export const Share = styled.div`
	margin-right: 25px;
`
export const ContainerBackArrow = styled.div`
	display: flex;
	align-items: center;
`
