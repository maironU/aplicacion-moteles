import styled from 'styled-components'

export const ContainerModal = styled.div`
  position: fixed;
  top: 0;
  width:100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
`
export const Modal = styled.div`
  position:fixed;
  background: white;
  width: 80%;
  height: auto;
  border-radius: 15px;
`