import React from 'react'
import { ContainerRomanticsPlans, ContainerRomanticPlan, ContainerTriangle, Triangle, ContainerInfoPlan, Plan } from './styled'

const RomanticsPlans = ({planes}) => {
	return(
		<ContainerRomanticsPlans>
			<span><b>Planes Románticos</b></span>
			{planes.map(function(item, key){
				return(
					<ContainerRomanticPlan key = {key}>
						<ContainerTriangle>
							<Triangle></Triangle>
						</ContainerTriangle>

						<ContainerInfoPlan>
							<Plan>{item.plan}</Plan>
						</ContainerInfoPlan>
					</ContainerRomanticPlan>
				)
			})}
		</ContainerRomanticsPlans>
	)
}

export default RomanticsPlans