import React, {useContext} from 'react'
import ReactDOM from 'react-dom'
import {TheContext} from '../../Context'
import {ContainerModal, Modal} from './styled'

const ModalTariffs = () => {

	let{setIsOpenModalTariffs, tarifas} = useContext(TheContext)

	const closeOpenModal = () => {
		var overflow = document.getElementById("overflow")
		overflow.style.overflow = "auto"
		overflow.style.position = "relative"
		setIsOpenModalTariffs(false)
	}

	return(
		<ContainerModal>
			<Modal>
				<div>Tarifas</div>
				{tarifas.map(function(item, key){
					return(
						<div>{item.tarifa}</div>
					)
				})}
				<div onClick={() => closeOpenModal()}>Salir</div>
			</Modal>
		</ContainerModal>
	)
}

export default ModalTariffs