import React, {useState, useEffect, useContext} from 'react'
import InfoBar from '../Components/InfoBar'
import {TheContext} from '../Context'
import GalleryImages from '../Components/GalleryImages'
import MotelInformation from '../Components/MotelInformation'
import TypesOfRooms from '../Components/TypesOfRooms'
import Services from '../Components/Services'
import RomanticsPlans from '../Components/RomanticsPlans'
import ModalTariffs from '../Components/ModalTariffs'

import {
  useParams
} from "react-router-dom";

const InfoMotel = () => {

	let {images, setImages, ip, setHabitacionImage, isOpenModalTariffs, price, setPrice} = useContext(TheContext)
	let { motel } = useParams()
	let nombreMotel = motel.replace(/-/gi, " ")

	useEffect(() => {
		window.fetch(ip + `api/imagenes/${nombreMotel}`)
		.then(function(res){
			return res.json()
		})
		.then(function(myJson){
			setImages(myJson)
			setHabitacionImage(myJson[0].imagenes)
			setPrice(myJson[0].precio)
		})

		return () => setImages([])
	},[])

	return(
		<div style = {{background: 'white'}}>
			<InfoBar motel={nombreMotel} />
			<GalleryImages/>
			{isOpenModalTariffs &&
				<ModalTariffs />
			}
			{images.length > 0 &&
				<>
					<MotelInformation informacion ={images[0].informacion}/>
					<TypesOfRooms tipos = {images[0].tipo_habitacion}/>
					<Services servicios = {images[0].servicios}/>
					<RomanticsPlans planes = {images[0].planes_romanticos}/>
				</>
			}
		</div>
	)
}

export default InfoMotel
