import styled from 'styled-components'

export const List = styled.ul`
	margin: 0;
	padding: 0;
	display: flex;
	flex-direction: row;
	align-items: center;
	overflow: scroll;
	margin-top: 44px;

	:last-child {
		margin-right: 11px;
	}

	::-webkit-scrollbar {
    	display: none;
	}
`
