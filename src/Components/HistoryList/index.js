import React, {useState, useEffect, useContext} from 'react'
import {List} from './styled'
import HistoryItem from '../HistoryItem'
import {TheContext} from '../../Context'

const HistoryList = () => {

	let {historyList, setHistoryList, filter, setNoPeticion, ip} = useContext(TheContext)

	useEffect(() => {

		const dato = JSON.stringify(filter.city_id) === undefined? '': JSON.stringify(filter.city_id)

		window.fetch(ip + `api/historiaMoteles/mostrar/${dato}`)
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
			setHistoryList(myJson);
		})
	},[filter])

	return(
		<List>
            { historyList.map( (elem, key ) => <HistoryItem key={key} {...elem}/> ) }
        </List>
	)
}

export default HistoryList

