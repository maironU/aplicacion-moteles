import React, {useContext} from 'react'
import { ContainerServices, ContainerAllServices, ContainerServicio, Imagen, Name } from './styled'
import {TheContext} from '../../Context'
const Services = ({servicios}) => {

	let {ip} = useContext(TheContext)
	return(
		<ContainerServices>
			<span><b>Servicios</b></span>
			<ContainerAllServices>
				{servicios.map(function(item, key){
					return(
						<ContainerServicio key = {key}>
							<Imagen src ={ ip + `api/almacenamiento/${item.imagen}`} />
							<Name>{item.servicio}</Name>
						</ContainerServicio>
					)
				})}
			</ContainerAllServices>
		</ContainerServices>
	)
}

export default Services