import React, {useEffect, useState, useContext} from 'react';
import Modal from '../Components/Modal'
import {TheContext} from '../Context'
import {
  useParams
} from "react-router-dom";

function History() {

	let { noPeticion,setNoPeticion, setHistorias, setHistoryMotel, toggleFullScreen, setToPlay, ip} =useContext(TheContext)

	let { motel } = useParams()

	let nombreMotel = motel.replace(/-/gi, " ")

	useEffect(() => {
		if(noPeticion === false){
			window.fetch(ip + `api/historiaMoteles/mostrar/${nombreMotel}`)
			.then(function(res){
				return res.json();
			})
			.then(function(myJson){
				setHistorias(myJson[0].historias);
				setHistoryMotel(myJson[0])
				setNoPeticion(true)
				setToPlay(false)
			})
		}	
	},[])

  return (
    <>  {noPeticion === true &&
    		<Modal />
    	}
    </>
  );
}

export default History;
