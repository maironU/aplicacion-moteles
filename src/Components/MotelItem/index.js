import React, {useContext} from 'react'
import {Link, Price, Image, Name, ContainerPrice,ContainerName, ContainerArrowRight} from './styled'
import { MdKeyboardArrowRight } from 'react-icons/md'
import {TheContext} from '../../Context'
import {useNearScreen} from '../../hooks/userNearScreen'

const MotelItem = ({price, urlImage,name}) => {
	 const options = {
            rootMargin: '0px 0px 300px 0px',
            threshold: 0,
        }

	const [ show, ref ] = useNearScreen(options)

	let {ip} = useContext(TheContext)


	return(
				<Link ref = {ref} to={`/${name.replace(/ /g, "-")}`}>	
					{show && 
						<>
							<Image src={ip + `api/almacenamiento/${urlImage}`} />

							<ContainerPrice>
								<Price>{price}</Price>
							</ContainerPrice>

							<ContainerName>
								<Name>{name}</Name>
								<ContainerArrowRight>
									<MdKeyboardArrowRight size={30}/>
								</ContainerArrowRight>
							</ContainerName>
						</>
					}
				</Link>
			)
}

export default MotelItem