import React, {useContext} from 'react'
import {ContainerItem, Image, Name, ContainerName, ContainerAll, Link} from './styled'
import {TheContext} from '../../Context'

const HistoryItem = ({motel, logo, historias, id}) => {

	let {setId, setIsOpenModal, setHistorias, setHistoryMotel, toggleFullScreen, setNoPeticion, ip} = useContext(TheContext)

	const showHistory = () => {
		toggleFullScreen()
		setHistorias(historias)
		setHistoryMotel({motel, logo})
		setIsOpenModal(true)
		setNoPeticion(true)
	}

	return(
		<ContainerItem>
			<ContainerAll>
				<Link onClick={(e) => showHistory()} to={`/historia/${motel.replace(/ /g, "-")}`}>
					<Image src = {ip + `api/almacenamiento/${logo}`} />
				</Link>
			</ContainerAll>

			<ContainerName>
				<Name>{motel}</Name>
			</ContainerName>		
		</ContainerItem>
	)
}

export default HistoryItem