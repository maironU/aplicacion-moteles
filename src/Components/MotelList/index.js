import React , {useContext, useState, useEffect} from 'react'
import {List} from './styled'
import MotelItem from '../MotelItem'
import {TheContext} from '../../Context'


const MotelList = () => {

	let {filter, MotelsForCity, setMotelsForCity, ip} = useContext(TheContext)

	useEffect(() => {

		const dato = JSON.stringify(filter.city_id) === undefined? '': JSON.stringify(filter.city_id)

		window.fetch(ip + `api/moteles/mostrar/${dato}`)
		.then(function(res){
			return res.json();
		})
		.then(function(myJson){
			setMotelsForCity(myJson);
		})
	},[filter])


	//`http://192.168.1.4:8000/api/almacenamiento/${motel.urlImage}`

	return(
		<List>
            { MotelsForCity.map( (elem, key ) => <MotelItem key={key} {...elem}/> ) }
        </List>
	)
}

export default MotelList