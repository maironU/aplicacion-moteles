import React, {useState, useContext} from 'react'
import Habitaciones from '../Habitaciones'
import {ContainerTipo, Tipo, ContainerNamePrice, Name, ContainerPriceTariffs, Price, Tariffs, ContainerHabitaciones} from './styled'
import {TheContext} from '../../Context'

const TypesOfRooms = ({tipos}) => {

	let {setIsOpenModalTariffs, setTarifas} = useContext(TheContext)

	const isOpenModal = ($tarifas) => {
		var overflow = document.getElementById("overflow")
		overflow.style.overflow = "hidden"
		overflow.style.position = "fixed"
		setIsOpenModalTariffs(true)
		setTarifas($tarifas)
	}

	return(
		<ContainerTipo>
			<span><b>Habitaciones</b></span>
			{tipos.map(function(item, key){
				return(
					<Tipo  key={key}>
						<ContainerNamePrice>
							<Name>
								{item.tipo_habitacion}	
							</Name>
							<ContainerPriceTariffs>
								<Price>
									<i>${item.tarifa_min}/h</i>
								</Price>
								<Tariffs onClick = {() => isOpenModal(item.tarifas)}><b>Tarifas</b></Tariffs>
							</ContainerPriceTariffs>
						</ContainerNamePrice>
						<ContainerHabitaciones>
							{item.habitaciones.map((item, key) => <Habitaciones key={key} {...item} />)}
						</ContainerHabitaciones>
					</Tipo>
				)
			})}
		</ContainerTipo>
	)
}

export default TypesOfRooms