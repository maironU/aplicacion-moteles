import styled from 'styled-components'

export const ContainerServices = styled.div`
	width: 95%;
	margin 0 auto;
`
export const ContainerAllServices = styled.div`
	width: 100%;
	display: flex;
	flex-flow: row wrap;
	margin-top: 10px;
	margin-bottom: 10px;
	justify-content: space-between;	
`
export const ContainerServicio = styled.div`
	width: 27%;
	background: #F0F0F0;
	display: flex;
	flex-direction: column;
	height: 110px;
	align-items: center;
	margin-bottom: 20px;
	border-radius: 5px;
	justify-content: space-around;
`
export const Imagen = styled.img`
	width: 80%;
	height: 60%;
`
export const Name = styled.span`
	font-size: 15px;
`