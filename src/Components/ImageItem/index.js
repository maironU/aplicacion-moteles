import React, {useContext} from 'react'
import {ContainerImageItem, Image} from './styled'
import {TheContext} from '../../Context'

const ImageItem = ({imagen, precio}) => {
	
	let {ip, price} = useContext(TheContext)

	return(
		<>
			<ContainerImageItem >
				<Image src={ip + `api/almacenamiento/${imagen}`} />
			</ContainerImageItem>
		</>
	)
}

export default ImageItem