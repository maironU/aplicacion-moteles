import React from 'react'
import { ContainerInformation, Information } from './styled'

const MotelInformation = ({informacion}) => {
	return(
		<ContainerInformation>
			<Information>
				{informacion}
			</Information>
		</ContainerInformation>
	)
}

export default MotelInformation