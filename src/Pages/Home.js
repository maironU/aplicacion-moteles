import React from 'react'
import SearchBar from '../Components/SearchBar'
import MotelList from '../Components/MotelList'
import HistoryList from '../Components/HistoryList'

const Home = () => {

	return(
		<React.Fragment>
				<SearchBar />
			
				<HistoryList />

				<MotelList />	
		</React.Fragment>
	)
}

export default Home
